<?php
class Page extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
  }

  function index(){
    $this->load->model('record_model');
    $data['konsumen'] =  $this->record_model->konsumen();
    $data['barang'] =  $this->record_model->barang();
    $this->load->view('admin/Beranda_admin',$data);
  }

  function data_konsumen(){
    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
      $this->load->view('admin/data_konsumen');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }

  function data_barang(){
    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
      $this->load->view('admin/data_barang');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }

  function data_transaksi(){
          $this->load->model('model_tampil');
          $this->load->model('model_transaksi');
              $data['transaksi'] = $this->model_transaksi->tampil_data();
    $data['pembeli']=$this->model_tampil->get_pembeli();
    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='3'){
      $this->load->view('admin/data_transaksi');
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
    }
  }
}
