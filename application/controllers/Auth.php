<?php
class Auth extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('login_model');
	}

	function index(){
		$this->load->view('form_login');
	}

	function login(){
        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

        $cek_admin=$this->login_model->auth_manager($username,$password);

        if($cek_admin->num_rows() > 0){ 
						$data=$cek_admin->row_array();
        		$this->session->set_userdata('masuk',TRUE);
		         if($data['level']=='1'){
		            $this->session->set_userdata('akses','1');
		            $this->session->set_userdata('ses_id',$data['id']);
		            $this->session->set_userdata('ses_nama',$data['nama']);
		            redirect('page');

		         }else{
		            $this->session->set_userdata('akses','2');
								$this->session->set_userdata('ses_id',$data['id']);
		            $this->session->set_userdata('ses_nama',$data['nama']);
		            redirect('page');
		         }

        }else{ 
					$cek_kasir=$this->login_model->auth_kasir($username,$password);
					if($cek_kasir->num_rows() > 0){
							$data=$cek_kasir->row_array();
        			$this->session->set_userdata('masuk',TRUE);
							$this->session->set_userdata('akses','3');
							$this->session->set_userdata('ses_id',$data['id']);
							$this->session->set_userdata('ses_nama',$data['nama']);
							redirect('page');
					}else{ 
							$url=base_url();
							$this->session->set_flashdata('msg','<div class="alert alert-danger 
								alert-dismissible fade show" role="alert">
	  							Username atau Passwoard Salah!!
	 							 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	   								 <span aria-hidden="true">&times;</span>
	 							 </button>
								</div>');
							redirect($url);
					}
        }

    }

    function logout(){
        $this->session->sess_destroy();
        $url=base_url('auth/login');
        redirect($url);
    }

}
