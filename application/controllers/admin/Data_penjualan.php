<?php 
class Data_penjualan extends CI_Controller{ 
public function index(){
	 	$this->load->model('model_penjualan');
	 	$data['penjualan'] = $this->model_penjualan->tampil_data()->result();
	    $this->load->view('templates_admin/header');
 		$this->load->view('templates_admin/sidebar');
 		$this->load->view('admin/data_penjualan', $data);
 		$this->load->view('templates_admin/footer');
}
public function tambah_aksi(){
	$this->load->model('model_penjualan');
	$tanggal		 = $this->input->post('tanggal');
	$total			 = $this->input->post('total');

	$data = array (
		'tanggal' => $tanggal,
		'total' => $total,
	);

	$this->model_penjualan->tambah_penjulan($data, 'tb_transaksi');
	$this->session->set_flashdata('message','<div class="alert alert-warning alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah ditambahkan</div>');
	redirect('admin/data_penjualan/index');
}

public function edit($id){
	$this->load->model('model_penjualan');
	$where = array('id' => $id);
	$data['penjualan'] = $this->model_penjualan->edit_penjualan($where, 'tb_transaksi')->result();
	$this->load->view('templates_admin/header');
	 $this->load->view('templates_admin/sidebar');
	 $this->load->view('templates_admin/footer');
	 $this->load->view('admin/edit_penjualan', $data);
}
public function update(){
	$this->load->model('model_penjualan');
	$id				=	$this->input->post('id');
	$tanggal		=	$this->input->post('tanggal');
	$total			=	$this->input->post('total');

$data = array(
	'tanggal' => $tanggal,
	'total' =>$total,
	);
$where= array(
	'id' =>$id
);

$this->model_penjualan->update_data($where,$data,'tb_transaksi');
$this->session->set_flashdata('message','<div class="alert alert-primary alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah diUpdate</div>');
redirect ('admin/data_penjualan/index');
}

public function hapus($id){
	$where = array ('id' => $id);
	$this->model_penjualan->hapus_data($where, 'tb_transaksi');
	$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah dihapus</div>');
	redirect('admin/data_penjualan/index');
}
public function detail($id){
	$this->load->model('model_penjualan');
	 $data['penjualan'] = $this->model_penjualan->detail_penjualan($id);
	 $this->load->view('templates_admin/header');
	 $this->load->view('templates_admin/sidebar');
	$this->load->view('templates_admin/footer');
	 $this->load->view('admin/detail_penjualan', $data);
 }


public function pdf(){
 		$this->load->model('model_penjualan');
 		$this->load->library('dompdf_gen');

 		$data['penjualan'] = $this->model_penjualan->tampil_data()->result();
 		$this->load->view('export_penjualan', $data);

 		$paper_size = 'A4';
 		$orientation = 'landscape';
 		$html = $this->output->get_output();
 		$this->dompdf->set_paper($paper_size, $orientation);

 		$this->dompdf->load_html($html);
 		$this->dompdf->render();
 		$this->dompdf->stream("baca_data.pdf", array('Attachment' =>0));
 	} 
public function search(){
 		$this->load->model('model_penjualan');
 		$keyword = $this->input->post('keyword');
 		$data['penjualan']=$this->model_penjualan->get_keyword($keyword);
 		$this->load->view('templates_admin/header');
 		$this->load->view('templates_admin/sidebar');
 		$this->load->view('admin/data_penjualan', $data);
 		$this->load->view('templates_admin/footer');
 	}
}
?>