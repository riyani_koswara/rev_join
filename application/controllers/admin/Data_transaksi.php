<?php 
class Data_transaksi extends CI_Controller{ 
		function __construct(){
		parent::__construct();
		$this->load->model('model_transaksi');
		$this->load->model('model_tampil');
	 	$this->load->model('model_baba');

	}
	 	public function index(){
		$data['transaksi'] = $this->model_transaksi->tampil_data();
		$data['pembeli']=$this->model_tampil->get_pembeli();
		$data['barang']=$this->model_baba->get_barang();
		$data['pilih']=$this->model_baba->tampil();
 		$this->load->view('admin/data_transaksi', $data);
}
public function transaksi()
{
	$data=$this->model_transaksi->tampil_data();
		echo json_encode($data);
}

function get_trans()
{
		$id_pembeli=$this->input->get('id');
		$data=$this->model_transaksi->get_transaksi($id_pembeli);
		echo json_encode($data);
}

public function listBarang(){
    $id_pembeli = $this->input->post('id_pembeli');
    
    $barang = $this->model_tampil->viewBarang($id_pembeli);
    $lists = "<option value=''>Pilih</option>";
    
    foreach($barang as $data){
      $lists .= "<option value='".$data->id."'>".$data->nama_barang."</option>";
    }
    
    $callback = array('list_barang'=>$lists);
    echo json_encode($callback);
  }

public function listHarga(){
    $id = $this->input->post('id');
    
    $harga = $this->model_tampil->viewBarang($id);
    $list = "<option value='Pilih'>";
    
    foreach($harga as $data){
      $list .= "<option value='".$data->harga."'>".$data->harga."</option>"; 
    }
    
    $callback = array('list_harga'=>$list);
    echo json_encode($callback);
  }




public function tambah_aksi(){
	  $this->load->library('form_validation');
	  $this->form_validation->set_rules('nama_pembeli', 'Nama Pembeli', 'required');
	  $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
	  $this->form_validation->set_rules('harga', 'Harga', 'required');
	  $this->form_validation->set_rules('jumlah', 'Jumlah', 'required|is_numeric');
	  $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
	  $this->form_validation->set_rules('total', 'Total', 'required|is_numeric');
	  if($this->form_validation->run())
 	 {
		$id_pembeli=$this->input->post('nama_pembeli');
  		$nama_pembeli=$this->input->post('nama_pembeli');
		$nama_barang=$this->input->post('nama_barang');
		$harga=$this->input->post('harga');
		$jumlah=$this->input->post('jumlah');
		$tanggal=$this->input->post('tanggal');
		$total=$this->input->post('total');
		$this->model_transaksi->simpan_transaksi($id_pembeli,$jumlah,$total,$tanggal);
	$data = array(
    'success' => '<div class="alert alert-success">Thank you!!!</div>'
   );
  }
  else
  {
   $data = array(
    'error'   => true,
    'name_error' => form_error('nama_pembeli'),
    'barang_error' => form_error('nama_barang'),
    'harga_error' => form_error('harga'),
    'jumlah_error' => form_error('jumlah'),
    'tanggal_error' => form_error('tanggal'),
    'total_error' => form_error('total')
   );
  }
  echo json_encode($data);
 }

 public function update(){
 	 $this->load->library('form_validation');
	  $this->form_validation->set_rules('nama_pembeli', 'Nama Pembeli', 'required');
	  $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
	  $this->form_validation->set_rules('harga', 'Harga', 'required');
	  $this->form_validation->set_rules('jumlah_edit', 'Jumlah', 'required|is_numeric');
	  $this->form_validation->set_rules('tanggal_edit', 'Tanggal', 'required');
	  $this->form_validation->set_rules('total_edit', 'Total', 'required|is_numeric');
	  if($this->form_validation->run())
 	 {
		$id_pembeli=$this->input->post('id_edit');
		$nama_pembeli=$this->input->post('nama_pembeli');
		$nama_barang=$this->input->post('nama_barang');
		$harga=$this->input->post('harga');
		$jumlah=$this->input->post('jumlah_edit');
		$total=$this->input->post('total_edit');
		$tanggal=$this->input->post('tanggal_edit');
		$this->model_transaksi->update_transaksi($id_pembeli,$jumlah,$total,$tanggal);
		$data = array(
    'success' => '<div class="alert alert-success">Thank you!!!</div>'
   );
  }
  else
  {
   $data = array(
    'error'   => true,
    'nali_edit_error' => form_error('nama_pembeli'),
    'nabar_edit_error' => form_error('nama_barang'),
    'harga_edit_error' => form_error('harga'),
    'jumlah_edit_error' => form_error('jumlah_edit'),
    'tanggal_edit_error' => form_error('tanggal_edit'),
    'total_edit_error' => form_error('total_edit')
   );
  }
  echo json_encode($data);
 }

function hapus()
{
		$id_pembeli=$this->input->post('id_pembeli');
		$data=$this->model_transaksi->hapus_transaksi($id_pembeli);
		echo json_encode($data);
}

/*public function hapus($id_pembeli){
	$this->load->model('model_transaksi');
	$where = array ('id_pembeli' => $id_pembeli);
	$this->model_transaksi->hapus_data($where, 'tb_transaksi');
	$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Berhasil!!</strong> Data Telah dihapus</div>');
	redirect('admin/data_transaksi');
}
*/
function get_brg()
    {
        $id_pembeli=$this->input->post('id_pembeli');
        $data=$this->model_transaksi->Barang($id_pembeli);
        echo json_encode($data);
    }

function detail_trans()
{
		$id_pembeli=$this->input->get('id');
		$data=$this->model_transaksi->detail_transaksi($id_pembeli);
		echo json_encode($data);
}

public function tambah_data(){
	  $this->load->library('form_validation');
	  $this->form_validation->set_rules('nama_pembeli', 'Nama Pembeli', 'required');
	  $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
	  $this->form_validation->set_rules('harga', 'Harga', 'required');
	  $this->form_validation->set_rules('jumlah', 'Jumlah', 'required|is_numeric');
	  $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
	  $this->form_validation->set_rules('total', 'Total', 'required|is_numeric');
	  if($this->form_validation->run())
 	 {
		$id_pembeli=$this->input->post('nama_pembeli');
  		$nama_pembeli=$this->input->post('nama_pembeli');
		$nama_barang=$this->input->post('nama_barang');
		$harga=$this->input->post('harga');
		$jumlah=$this->input->post('jumlah');
		$tanggal=$this->input->post('tanggal');
		$total=$this->input->post('total');
		$this->model_transaksi->simpan_transaksi($id_pembeli,$jumlah,$total,$tanggal);
	$data = array(
    'success' => '<div class="alert alert-success">Thank you!!!</div>'
   );
  }
  else
  {
   $data = array(
    'error'   => true,
    'name_error' => form_error('nama_pembeli'),
    'barang_error' => form_error('nama_barang'),
    'harga_error' => form_error('harga'),
    'jumlah_error' => form_error('jumlah'),
    'tanggal_error' => form_error('tanggal'),
    'total_error' => form_error('total')
   );
  }
  echo json_encode($data);
 }
}
?>