<?php 
class Data_barang extends CI_Controller{ 
function __construct(){
		parent::__construct();
		$this->load->model('model_barang');
	}
public function index()
{
	    $this->load->view('menu');
 		$this->load->view('admin/data_barang');}

public function data_brg()
{
	$data=$this->model_barang->barang_list();
		echo json_encode($data);
}

function get_brg()
{
		$dibar=$this->input->get('id');
		$data=$this->model_barang->get_barang_by_kode($dibar);
		echo json_encode($data);
}

function simpan_brg()
 {
  $this->load->library('form_validation');
  $this->form_validation->set_rules('nama', 'Nama Barang', 'required');
  $this->form_validation->set_rules('harga', 'Harga', 'required|is_numeric');
  $this->form_validation->set_rules('stok', 'Stok', 'required|is_numeric');
  $this->form_validation->set_rules('ktg', 'Kategori', 'required');
  $this->form_validation->set_rules('tg', 'Tanggal', 'required');
  if($this->form_validation->run())
  {
  	$dibar=$this->input->post('dibar');
		$nama=$this->input->post('nama');
		$harga=$this->input->post('harga');
		$stok=$this->input->post('stok');
		$ktg=$this->input->post('ktg');
		$tg=$this->input->post('tg');
  	$this->model_barang->simpan_barang($dibar,$nama,$harga,$stok,$ktg,$tg);
   $data = array(
    'success' => '<div class="alert alert-success">Thank you!!!</div>'
   );
  }
  else
  {
   $data = array(
    'error'   => true,
    'name_error' => form_error('nama'),
    'harga_error' => form_error('harga'),
    'stok_error' => form_error('stok'),
    'kategori_error' => form_error('ktg'),
    'tanggal_error' => form_error('tg')
   );
  }
  echo json_encode($data);
 }

function update_brg()
{
  $this->load->library('form_validation');
  $this->form_validation->set_rules('nama_edit', 'Nama Barang', 'required');
  $this->form_validation->set_rules('harga_edit', 'Harga', 'required|is_numeric');
  $this->form_validation->set_rules('stok_edit', 'Stok', 'required|is_numeric');
  $this->form_validation->set_rules('ktg_edit', 'Kategori', 'required');
  $this->form_validation->set_rules('tg_edit', 'Tanggal', 'required');
  if($this->form_validation->run())
  {
		$dibar=$this->input->post('id_edit');
		$nama=$this->input->post('nama_edit');
		$harga=$this->input->post('harga_edit');
		$stok=$this->input->post('stok_edit');
		$ktg=$this->input->post('ktg_edit');
		$tg=$this->input->post('tg_edit');
		$this->model_barang->update_barang($dibar,$nama,$harga,$stok,$ktg,$tg);
 $data = array(
    'success' => '<div class="alert alert-success">Thank you!!!</div>'
   );
  }
  else
  {
   $data = array(
    'error'   => true,
    'nama_edit_error' => form_error('nama_edit'),
    'harga_edit_error' => form_error('harga_edit'),
    'stok_edit_error' => form_error('stok_edit'),
    'ktg_edit_error' => form_error('ktg_edit'),
    'tg_edit_error' => form_error('tg_edit')
   );
  }
  echo json_encode($data);
 }

function hapus_brg()
{
		$dibar=$this->input->post('id');
		$data=$this->model_barang->hapus_barang($dibar);
		echo json_encode($data);
}
 function detail($id){
	$this->load->model('model_barang');
	 $data['barang'] = $this->model_barang->detail_barang($id);
 }


public function pdf(){
 		$this->load->model('model_barang');
 		$this->load->library('dompdf_gen');

 		$data['barang'] = $this->model_barang->barang_list();
 		$this->load->view('export_barang', $data);

 		$paper_size = 'A4';
 		$orientation = 'landscape';
 		$html = $this->output->get_output();
 		$this->dompdf->set_paper($paper_size, $orientation);

 		$this->dompdf->load_html($html);
 		$this->dompdf->render();
 		$this->dompdf->stream("baca_data.pdf", array('Attachment' =>0));
 	}

public function print(){
 		$this->load->model('model_barang');
 		$data['barang'] = $this->model_barang->barang_list();
 		$this->load->view('print_barang', $data);
 	}  
public function search(){
 		$this->load->model('model_barang');
 		$keyword = $this->input->post('keyword');
 		$data['barang']=$this->model_barang->get_keyword($keyword);
 		$this->load->view('templates_admin/header');
 		$this->load->view('templates_admin/sidebar');
 		$this->load->view('admin/data_barang', $data);
 		$this->load->view('templates_admin/footer');
 	}

public function excel(){
		$data['barang'] = $this->model_barang->barang_list();

		require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

		$object = new PHPExcel();

		$object->getProperties()->setCreator("Riyani Koswara");
		$object->getProperties()->setLastModifiedBy("Riyani Koswara");
		$object->getProperties()->setTitle("Data Barang");

		$object->setActiveSheetIndex(0);

		$object->getActiveSheet()->setCellValue('A1', 'NO');
		$object->getActiveSheet()->setCellValue('B1', 'NAMA BARANG');
		$object->getActiveSheet()->setCellValue('C1', 'HARGA');
		$object->getActiveSheet()->setCellValue('D1', 'STOK');
		$object->getActiveSheet()->setCellValue('E1', 'KATEGORI');
		$object->getActiveSheet()->setCellValue('F1', 'TANGGAL INPUT');

		$baris= 2;
		$no = 1;

		foreach ($data['barang'] as $brg) {
			$object->getActiveSheet()->setCellValue('A'.$baris, $no++);
			$object->getActiveSheet()->setCellValue('B'.$baris, $brg->nama_barang);
			$object->getActiveSheet()->setCellValue('C'.$baris, $brg->harga);
			$object->getActiveSheet()->setCellValue('D'.$baris, $brg->stok);
			$object->getActiveSheet()->setCellValue('E'.$baris, $brg->kategori);
			$object->getActiveSheet()->setCellValue('F'.$baris, $brg->tanggal_input);

			$baris++;
		}
		$filename= 'Data_barang'.'.xlsx';
		$object->getActiveSheet()->setTitle("Data  Barang");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer=PHPExcel_IOFactory::createwriter($object, 'Excel2007');
		$writer->save('php://output');

		exit;

}
}
?>