<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Select extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form'));
        $this->load->database();
        $this->load->model('model_select');
    }
    function index()
    {
        $data['selec'] = $this->model_select->nama_pembeli();
        $this->load->view('admin/data_transaksi',$data);
    }
    function barang()
    {
        $id = $this->input->post('id');
        $data['selec'] = $this->model_select->barang($id);
        $this->load->view('admin/data_transaksi',$data);
    }
}