<?php 
class Data_konsumen extends CI_Controller{ 
	function __construct(){
		parent::__construct();
		$this->load->model('model_pembeli');
	}
public function index()
{
	   $this->load->view('menu');
 		$this->load->view('admin/data_konsumen');
}

public function data_konsum()
{
	$data=$this->model_pembeli->pembeli_list();
		echo json_encode($data);
}

function get_konsum()
{
		$kode=$this->input->get('id');
		$data=$this->model_pembeli->get_pembeli_by_kode($kode);
		echo json_encode($data);
}

function simpan_konsum()
{
  $this->load->library('form_validation');
  $this->form_validation->set_rules('nama', 'Nama Pembeli', 'required');
  $this->form_validation->set_rules('alam', 'Alamat', 'required');
  $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[tb_pembeli.email]');
  $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
  if($this->form_validation->run())
  {
		$kode=$this->input->post('kode');
		$nama=$this->input->post('nama');
		$alam=$this->input->post('alam');
		$jk=$this->input->post('jk'); 
		$email=$this->input->post('email'); 
		$this->model_pembeli->simpan_pembeli($kode,$nama,$alam,$jk,$email);
	   $data = array(
    'success' => '<div class="alert alert-success">Thank you!!!</div>'
   );
  }
  else
  {
   $data = array(
    'error'   => true,
    'name_error' => form_error('nama'),
    'alamat_error' => form_error('alam'),
    'email_error' => form_error('email'),
    'jenis_error' => form_error('jk')
   );
  }
  echo json_encode($data);
 }

function update_pembeli()
{
  $this->load->library('form_validation');
  $this->form_validation->set_rules('nama_edit', 'Nama Pembeli', 'required');
  $this->form_validation->set_rules('alam_edit', 'Alamat', 'required');
  $this->form_validation->set_rules('email_edit', 'Email', 'required|valid_email');
  $this->form_validation->set_rules('jk_edit', 'Jenis Kelamin', 'required');
  if($this->form_validation->run())
  {
		$kode=$this->input->post('id_edit');
		$nama=$this->input->post('nama_edit');
		$alam=$this->input->post('alam_edit');
		$jk=$this->input->post('jk_edit'); 
		$email=$this->input->post('email_edit');
		$this->model_pembeli->update($kode,$nama,$alam,$jk,$email);
		$data = array(
    'success' => '<div class="alert alert-success">Thank you!!!</div>'
   );
  }
  else
  {
   $data = array(
    'error'   => true,
    'nama_edit_error' => form_error('nama_edit'),
    'alam_edit_error' => form_error('alam_edit'),
    'email_edit_error' => form_error('email_edit'),
    'jk_edit_error' => form_error('jk_edit')
   );
  }
  echo json_encode($data);
 }

function hapus_pembeli()
{
		 $kode = $this->input->post('id_pembeli');
         $data = $this->model_pembeli->hapus_pembeli($kode);
         echo json_encode($data);
}

public function detail($kode){
 		$data['pembeli'] = $this->model_pembeli->detail_pembeli($kode);
 		$this->load->view('templates_admin/header');
 		$this->load->view('templates_admin/sidebar');
 		$this->load->view('admin/data_konsumen', $data);
 		$this->load->view('templates_admin/footer');
 	}


public function pdf(){
 		$this->load->model('model_pembeli');
 		$this->load->library('Dompdf_gen');

 		$data['konsumen'] = $this->model_pembeli->pembeli_list();
 		$this->load->view('export_konsumen', $data);

 		$paper_size = 'A4';
 		$orientation = 'landscape';
 		$html = $this->output->get_output();
 		$this->dompdf->set_paper($paper_size, $orientation);

 		$this->dompdf->load_html($html);
 		$this->dompdf->render();
 		$this->dompdf->stream("baca_data.pdf", array('Attachment' =>0));
	 } 

public function excel(){
    $data['konsumen'] = $this->model_pembeli->pembeli_list();

    require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel.php');
    require(APPPATH. 'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

    $object = new PHPExcel();

    $object->getProperties()->setCreator("Riyani Koswara");
    $object->getProperties()->setLastModifiedBy("Riyani Koswara");
    $object->getProperties()->setTitle("Data Konsumen");

    $object->setActiveSheetIndex(0);

    $object->getActiveSheet()->setCellValue('A1', 'NO');
    $object->getActiveSheet()->setCellValue('B1', 'NAMA PEMBELI');
    $object->getActiveSheet()->setCellValue('C1', 'ALAMAT');
    $object->getActiveSheet()->setCellValue('D1', 'EMAIL');
    $object->getActiveSheet()->setCellValue('E1', 'JENIS KELAMIN');

    $baris= 2;
    $no = 1;

    foreach ($data['konsumen'] as $ksm) {
      $object->getActiveSheet()->setCellValue('A'.$baris, $no++);
      $object->getActiveSheet()->setCellValue('B'.$baris, $ksm->nama_pembeli);
      $object->getActiveSheet()->setCellValue('C'.$baris, $ksm->alamat);
      $object->getActiveSheet()->setCellValue('D'.$baris, $ksm->email);
      $object->getActiveSheet()->setCellValue('E'.$baris, $ksm->jenis_kelamin);

      $baris++;
    }
    $filename= 'Data_barang'.'.xlsx';
    $object->getActiveSheet()->setTitle("Data Konsumen");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $writer=PHPExcel_IOFactory::createwriter($object, 'Excel2007');
    $writer->save('php://output');

    exit;

}  

public function print(){
    $this->load->model('model_pembeli');
    $data['konsumen'] = $this->model_pembeli->pembeli_list();
    $this->load->view('print_pembeli', $data);
   }  
	 
	 public function search(){
		$this->load->model('model_pembeli');
		$keyword = $this->input->post('keyword');
		$data['konsumen']=$this->model_pembeli->get_keyword($keyword);
		$this->load->view('templates_admin/header');
		$this->load->view('templates_admin/sidebar');
		$this->load->view('admin/data_konsumen', $data);
		$this->load->view('templates_admin/footer');
	}

}
?>