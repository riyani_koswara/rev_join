<body>
    <div class="wrapper">
        <nav id="sidebar">
            <div class="sidebar-header">
            <hr color="white" />
            <ul class="list-unstyled components">
                <div class="foto">
                <img src="<?php echo base_url('template/images/siswa.png') ?>" height = "150px" width="150px" class="img-circle">
            </div>
            <hr color="white" />

                <li class="active">
                    <a href="<?php echo base_url('beranda_admin/index')?>">
                        <i class="fas fa-home"></i>
                        Dashboard
                    </a>
                    </li>
                <li>
                    <a href="<?php echo base_url('admin/data_konsumen')?>">
                    <i class="fas fa-users"></i>
                        Data Konsumen
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/data_barang')?>">
                    <i class="fas fa-box"></i>
                        Data Barang
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/data_transaksi') ?>">
                    <i class="fas fa-hand-holding-usd"></i>
                        Data Transaksi
                    </a>
                </li>
                    </ul>
                        <li><a href="<?php echo base_url().'auth/logout'?>">Sign Out</a></li>

        </nav>

        <!-- Page Content  -->
        <div id="content">
    <!-- jQuery CDN-->
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.dataTables.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>

</html>