  <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Crud Join - C O B A</title>

        <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
  <!-- Ionicons -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/jquery.dataTables.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/sweetalert/sweetalert.css') ?>">
    <script type=”text/javascript” src=<?php echo base_url('js/jquery-1.4.2.js'); ?>></script>
    <script src=“js/jquery.validationEngine-en.js” type=”text/javascript”></script>
    <script src=“js/jquery.validationEngine.js” type=”text/javascript”></script>
    <link rel=”stylesheet” href=“css/validationEngine.jquery.css” type=”text/css” media=”screen” title=”no title” charset=”utf-8″ />
    <link rel=”stylesheet” href=“css/template.css” type=”text/css” media=”screen” title=”no title” charset=”utf-8″ />

     <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
</head>
<body>
    <div class="wrapper">
        <nav id="sidebar">
            <div class="sidebar-header">
            <hr color="white" />
            <ul class="list-unstyled components">
                <div class="foto">
                <img src="<?php echo base_url('template/images/siswa.png') ?>" height = "150px" width="150px" class="img-circle">
                <h4>Selamat Datang <?php echo $this->session->userdata('ses_nama');?>!</h4>
            </div>
            <hr color="white" />

              <?php if($this->session->userdata('akses')=='1'):?>
      <li><a href="<?php echo base_url().'page/index'?>"><i class="fas fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo base_url().'page/data_barang'?>"> <i class="fas fa-box"></i> Data Barang</a></li>
      <li><a href="<?php echo base_url().'page/data_konsumen'?>"><i class="fas fa-users"></i> Data Kosumen</a></li>
      <li><a href="<?php echo base_url().'admin/data_transaksi'?>"><i class="fas fa-hand-holding-usd"></i> Data Transaksi</a></li>
  <?php elseif($this->session->userdata('akses')=='2'):?>
      <li><a href="<?php echo base_url().'page/index'?>"><i class="fas fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo base_url().'page/data_barang'?>"> <i class="fas fa-box"></i> Data Barang</a></li>
      <li><a href="<?php echo base_url().'page/data_konsumen'?>"><i class="fas fa-users"></i> Data Kosumen</a></li>
  <?php else:?>
    <li><a href="<?php echo base_url().'page/index'?>"><i class="fas fa-home"></i> Dashboard</a></li>
    <li><a href="<?php echo base_url().'admin/data_transaksi'?>"><i class="fas fa-hand-holding-usd"></i> Data Transaksi</a></li>
  <?php endif;?>
                      </ul>
                        <li><a href="<?php echo base_url().'auth/logout'?>"><i class="fas fa-sync-alt"></i>  Sign Out</a></li>

        </nav>

        <!-- Page Content  -->
        <div id="content">
    <!-- jQuery CDN-->
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.dataTables.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>

</html>