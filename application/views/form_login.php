<!DOCTYPE html>
<html>
<head>
<title>CRUD Log In</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="User Icon Login Form Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo base_url('login/css/style.css') ?>" type="text/css" media="all" />
</head>
<body>

    <div class="container">
      <div class="contact-form">
   <div class="profile-pic">
          <img src="<?php echo base_url('template/images/siswa.png') ?>" style="width: 150px; height: 150px;"/>
   </div>  
   <div class="signin">                         
  <?php echo $this->session->flashdata('msg');?>
    <form method="post" action="<?php echo base_url('auth/login') ?>">
      <form>
        <input type="text" name="username" class="user" placeholder="Your Username "  onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Username';}" />
          <?php echo form_error('username', '<div class="text-danger small ml-2">',
                                            '</div>'); ?>
           <input name="password" type="password" class="pass"  placeholder="Your Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" />
                  <?php echo form_error('passwoard', '<div class="text-danger small ml-2">','</div>'); ?>
                 <p> Not register ?</p> <a href="<?php echo base_url(''); ?>" >click here </a> 
                    <button type="submit">Login</button>
                  </form>
                      </div>
                    </div>
</body>
</html>