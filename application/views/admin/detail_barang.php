<div class="card mb-3">
  <div class="card-header text-center">   
   <strong>Detail Data Barang</strong>
   <div class="class" style="float: right;">
          <?php echo anchor('admin/data_barang/index/','<div class="btn btn-warning"><i class="fas fa-hand-point-left"></i></div>') ?>
     </div>
   </div>
  <div class="card-body">
    <div class="row text-center">
    <?php foreach ($barang as $brg) : ?>
    		<div class="col-md-12">
    			<table class="table">
    				<tr>
    					<h6>Nama Barang : <?php echo $brg->nama_barang ?></h6>
              <h6>Harga : <?php echo $brg->harga ?></h6>
              <h6>Stok : <?php echo $brg->stok ?></h6>
              <h6>Kategori : <?php echo $brg->kategori ?></h6>
    				</tr>
    			</table>
    		</div>
    	</div>

    <?php endforeach; ?>
    </div>
      </div>
</div>