    <div class="container-fluid">
       <?php $this->load->view('menu');?>
     <nav class="navbar">
         <h3><i class="fas fa-users" style="color: white;"></i>  Data Konsumen</h3>
           <div class="card-tools">
      <a class="btn btn-primary btn-sm" href="<?php echo base_url('admin/data_konsumen/print') ?>">
      <i class="fas fa-print"></i> Print</a>
      <a class="btn btn-warning btn-sm" href="<?php echo base_url('admin/data_konsumen/pdf') ?>">
      <i class="fas fa-file"></i> Export PDF</a>
      <a class="btn btn-success btn-sm" href="<?php echo base_url('admin/data_konsumen/excel') ?>">
      <i class="fas fa-file-excel"></i> Export Excel</a>
      <div class="pull-right"><a href="#" class="btn btn-sm btn-default" data-toggle="modal" data-target="#ModalKonsumen"><span class="fa fa-plus"></span> Tambah Data</a></div>
          </div>
        </nav>

	<div class="row">
    <div class="col-lg-12">
      <div id="reload">
       <table class="table table-striped mt-4" id="mydata">
        <thead>
		<tr>
	  <th>Nama Konsumen</th>
	  <th>Alamat</th>
    <th>Jenis Kelamin</th>
      <th>Email</th>
      <th>Aksi</th>
		</tr>
        </thead>
        <tbody id="show_data">
        
      </tbody>
      </table>
    </div>
  </div>
</div>
    
        <!-- MODAL ADD -->
        <div class="modal fade" id="ModalKonsumen" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">INPUT Data Konsumen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" id="pembeli_form">
      <div class="modal-body">
                    <div class="form-group">
                                   <div class="form-group">
              <label>Nama Produk</label>
              <input class="form-control" type="text" name="nama_produk" value="<?php echo (!empty($produk[0]->nama_produk)) ? $produk[0]->nama_produk : ''?>" required>
            </div>
            <div class="form-group">
              <label>Kategori</label>
              <select class="form-control" name="kategori" required>
                <option value="" selected>Pilih Kategori</option>
                <?php foreach ($kategori as $key => $k):
                  $kid = (!empty($produk[0]->kategori_id)) ? $produk[0]->kategori_id : '';
                  if ($kid == $k->kategori_id) {
                    echo '<option value="'.$kid.'" selected>'.$k->nama_kategori.'</option>';
                  }else {
                    echo '<option value="'.$k->kategori_id.'">'.$k->nama_kategori.'</option>';
                  }
                 endforeach; ?>
              </select>
            </div>
            <div class="form-group">
              <label>Harga</label>
              <input class="form-control" type="text" name="harga" data-parsley-type="number" value="<?php echo (!empty($produk[0]->harga)) ? $produk[0]->harga : ''?>" required>
            </div>
            <div class="form-group">
              <label>Stok</label>
              <input class="form-control" type="text" name="stok" data-parsley-type="number" value="<?php echo (!empty($produk[0]->stok)) ? $produk[0]->stok : ''?>" required>
            </div>
            <div class="form-group">
              <label>Deskripsi</label>
              <textarea class="form-control" type="text" name="deskripsi" required><?php echo (!empty($produk[0]->deskripsi)) ? $produk[0]->deskripsi : ''?></textarea>
            </div>
            <div class="form-group">
              <label></label>
              <button class="btn btn-info" type="submit">Simpan</button>
              <a class="btn btn-default" href="<?php echo base_url('data_produk')?>">Kembali</a>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label>Gambar</label>
              <input type="file" name="gambar" accept="image/*" required><br>
              <?php if (!empty($produk[0]->gambar)): ?>
                <img src="<?php echo base_url('upload/'.$produk[0]->gambar);?>" width="100" alt="">
              <?php endif; ?>
            </div>
          </div>

            </div>
        </div>
        <!--END MODAL ADD-->

        <!-- MODAL EDIT -->
        <div class="modal fade" id="ModalKonEdit" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Edit Data Konsumen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" id="pembeli_edit">
      <div class="modal-body">
                  <div class="form-group">
                        <label class="control-label col-xs-3">No</label>
                            <input name="id_edit" id="id_pembeli2" class="form-control" type="text" placeholder="No Konsumen" style="width:335px;" readonly>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3">Nama</label>
                            <input name="nama_edit" id="nama_pembeli2" class="form-control" type="text" placeholder="Nama Konsumen" style="width:335px;">
                             <span id="nama_edit_error" class="help-block"></span>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3">Alamat</label>
                            <input name="alam_edit" id="alamat2" class="form-control" type="text" placeholder="Alamat" style="width:335px;">
                             <span id="alam_edit_error" class="help-block"></span>
                        </div>

                         <div class="form-group">
                        <label class="control-label col-xs-3">Email</label>
                            <input name="email_edit" id="email2" class="form-control" type="email" placeholder="Email" style="width:335px;">
                             <span id="email_edit_error" class="help-block"></span>
                    </div>

                    <div class="form-group">
                       <label class="control-label col-xs-3">Jenis Kelamin : </label>
                               <select id="jenis_kelamin2" name="jk_edit">
                                <option value="">Pilih</option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                          <span id="jk_edit_error" class="help-block"></span>
                          </div>
                      </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <input type="submit" name="btn_update" id="btn_update" class="btn btn-info" value="Update">
                </div>
            </form>
            </div>
            </div>
        </div>
    </div>
        <!--END MODAL EDIT-->

        <!--MODAL HAPUS-->
        <!-- <div class="modal fade" id="ModalKonHapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="form-horizontal">
                      <div class="modal-body">
                        <input type="hidden" name="id_pembeli" id="textkode" value="">
                    <div class="alert alert-warning">  Apakah Anda yakin mau menghapus data ini?</i>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div> -->
        <!--END MODAL HAPUS-->

                <!-- Modal Detail -->
<div class="modal fade" id="ModalDetail" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Detail Data Konsumen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                  <div class="form-group">
                        <label class="control-label col-xs-3" >No</label>
                            <input name="id_detail" class="form-control" type="text" placeholder="No Konsumen" style="width:335px;" readonly>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama</label>
                            <input name="nama_detail" class="form-control" type="text" placeholder="Nama Konsumen" style="width:335px;" readonly>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Alamat</label>
                            <input name="alam_detail" class="form-control" type="text" placeholder="Alamat" style="width:335px;"readonly>
                        </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Email</label>
                            <input name="email_detail" class="form-control" type="email" placeholder="Email" style="width:335px;"readonly>
                    </div>

                    <div class="form-group">
                       <label class="control-label col-xs-3">Jenis Kelamin : </label>
                               <select name="jk_detail" readonly>
                                <option value="">Pilih</option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                          </div>
                      </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                </div>
            </form>
            </div>
            </div>
        </div>
    </div>
        <!-- akhir kode modal dialog -->

<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.dataTables.js'?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    tampil_data_barang(); //pemanggilan fungsi tampil barang.
    
    $('#mydata').dataTable();
     
    //fungsi tampil barang
    function tampil_data_barang(){
        $.ajax({
            type  : 'GET',
            url   : '<?php echo base_url()?>admin/data_konsumen/data_konsum',
            async : true,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                          '<td>'+data[i].nama_pembeli+'</td>'+
                            '<td>'+data[i].alamat+'</td>'+
                            '<td>'+data[i].jenis_kelamin+'</td>'+
                            '<td>'+data[i].email+'</td>'+
                            '<td style="text-align:right;">'+
                                    '<a href="javascript:;" class="btn btn-info btn-sm item_edit" data="'+data[i].id_pembeli+'"><i class="fas fa-pen-square"></i></a>'+' '+
                                    '<a href="javascript:;" class="btn btn-danger btn-sm item_hapus" data="'+data[i].id_pembeli+'"><i class="fas fa-trash"></i></a>'+' '+
                                    '<a href="javascript:;" onclick="ModalDetail" class="btn btn-success btn-sm item_detail" data="'+data[i].id_pembeli+'"><i class="fas fa-search-plus"></i></a>'+' '+
                                '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });
    }

    //GET UPDATE
    $('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('admin/data_konsumen/get_konsum')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                  $.each(data,function(id_pembeli, nama_pembeli, jenis_kelamin, alamat, email){
                  $('#ModalKonEdit').modal('show');
                  $('[name="id_edit"]').val(data.id_pembeli);
                  $('[name="nama_edit"]').val(data.nama_pembeli);
                  $('[name="alam_edit"]').val(data.alamat);
                  $('[name="jk_edit"]').val(data.jenis_kelamin);
                  $('[name="email_edit"]').val(data.email);
                });
                }
            });
            return false;
        });

 //GET Detail
    $('#show_data').on('click','.item_detail',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('admin/data_konsumen/get_konsum')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                  $.each(data,function(id_pembeli, nama_pembeli, alamat, jenis_kelamin, email){
                  $('#ModalDetail').modal('show');
                  $('[name="id_detail"]').val(data.id_pembeli);
                  $('[name="nama_detail"]').val(data.nama_pembeli);
                  $('[name="alam_detail"]').val(data.alamat);
                  $('[name="email_detail"]').val(data.email);
                  $('[name="jk_detail"]').val(data.jenis_kelamin);
                });
                }
            });
            return false;
        });

    //GET HAPUS
    $('#show_data').on('click','.item_hapus',function(){
            var id=$(this).attr("data");
            console.log(id);
            swal({
                title: 'Konfirmasi',
                text: "Anda ingin menghapus ",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                cancelButtonText: 'Tidak',
                reverseButtons: true
              }, function(hapus){
                if (hapus) {
                  $.ajax({
                    url  : "<?php echo base_url('admin/data_konsumen/hapus_pembeli')?>", 
                    method:"post",
                    data:{id_pembeli:id},
                    beforeSend :function () {
                    swal({
                        title: 'Menunggu',
                        html: 'Memproses data',
                        onOpen: () => {
                          swal.showLoading()
                        }
                      })      
                    },    
                    success:function(data){
                      swal(
                        'Hapus',
                        'Berhasil Terhapus',
                        'success'
                      )
                      tampil_data_barang();                   
                       }
                  })
              } else {
                  swal(
                    'Batal',
                    'Anda membatalkan penghapusan',
                    'error'
                  )
                }
              })
            });

    //Simpan Barang
        $('#pembeli_form').on('submit', function(event){
  event.preventDefault();
            var kode=$('#id_pembeli').val();
            var nama=$('#nama_pembeli').val();
            var alam=$('#alamat').val();
            var jk=$('#jenis_kelamin').val();
            var email=$('#email').val();
  $.ajax({
   url  : "<?php echo base_url('admin/data_konsumen/simpan_konsum')?>",
   method:"POST",
   data:$(this).serialize(),
   dataType:"json",
   beforeSend:function(){
    swal({
                  title: 'Menunggu',
                  html: 'Memproses data',
                  onOpen: () => {
                    swal.showLoading()
                  }
                })
     $('#btn_simpan').attr('disabled', 'disabled');
   },
   success:function(data)
   {
    if(data.error)
    {
     if(data.name_error != '')
     {
      $('#name_error').html(data.name_error);
     }
     else
     {
      $('#name_error').html('');
     }
     if(data.alamat_error != '')
     {
      $('#alamat_error').html(data.alamat_error);
     }
     else
     {
      $('#alamat_error').html('');
     }
     if(data.email_error != '')
     {
      $('#email_error').html(data.email_error);
     }
     else
     {
      $('#email_error').html('');
     }
     if(data.jenis_error != '')
     {
      $('#jenis_error').html(data.jenis_error);
     }
     else
     {
      $('#jenis_error').html('');
     }
    }
    if(data.success)
    {
        swal({
                  type: 'success',
                  title: 'Tambah Barang',
                  text: 'Anda Berhasil Menambah Pembeli'
                })
     $('#success_message').html(data.success);
     $('#name_error').html('');
     $('#alamat_error').html('');
     $('#email_error').html('');
     $('#jenis_error').html('');
     $('#pembeli_form')[0].reset();
     $('#ModalKonsumen').modal('hide');
     tampil_data_barang();
    }
    $('#btn_simpan').attr('disabled', false);
   }
  })
 });


        //Update Barang
        $('#pembeli_edit').on('submit', function(event){
  event.preventDefault();
            var kode=$('#id_pembeli2').val();
            var nama=$('#nama_pembeli2').val();
            var alam=$('#alamat2').val();
            var jk=$('#jenis_kelamin2').val();
            var email=$('#email2').val();
            console.log($(this).serialize());
  $.ajax({
   url  : "<?php echo base_url('admin/data_konsumen/update_pembeli')?>",
   method:"POST",
   data:$(this).serialize(),
   dataType:"json",
   beforeSend:function(){
    swal({
                  title: 'Menunggu',
                  html: 'Memproses data',
                  onOpen: () => {
                    swal.showLoading()
                  }
                })
     $('#btn_update').attr('disabled', 'disabled');
   },
   success:function(data)
   {
    if(data.error)
    {
     if(data.nama_edit_error != '')
     {
      $('#nama_edit_error').html(data.nama_edit_error);
     }
     else
     {
      $('#nama_edit_error').html('');
     }
     if(data.alam_edit_error != '')
     {
      $('#alam_edit_error').html(data.alam_edit_error);
     }
     else
     {
      $('#alam_edit_error').html('');
     }
     if(data.email_edit_error != '')
     {
      $('#email_edit_error').html(data.email_edit_error);
     }
     else
     {
      $('#email_edit_error').html('');
     }
     if(data.jk_edit_error != '')
     {
      $('#jk_edit_error').html(data.jk_edit_error);
     }
     else
     {
      $('#jk_edit_error').html('');
     }
    }
    if(data.success)
    {
      swal({
          type: 'success',
          title: 'Tambah Barang',
          text: 'Anda Berhasil Mengubah Pembeli'
          })
     $('#success_message').html(data.success);
     $('#nama_edit_error').html('');
     $('#alam_edit_error').html('');
     $('#email_edit_error').html('');
     $('#jk_edit_error').html('');
     $('#pembeli_edit')[0].reset();
     $('#ModalKonEdit').modal('hide');
        tampil_data_barang();
    }
    $('#btn_update').attr('disabled', false);
   }
  })
  return false;
 });
      });
</script>