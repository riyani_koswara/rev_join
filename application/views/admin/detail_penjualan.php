<div class="card mb-3">
  <div class="card-header text-center">   
   <strong>Detail Data Penjualan</strong>
   <div class="class" style="float: right;">
          <?php echo anchor('admin/data_penjualan/index/','<div class="btn btn-warning"><i class="fas fa-hand-point-left"></i></div>') ?>
     </div>
   </div>
  <div class="card-body">
    <div class="row text-center">
    <?php foreach ($penjualan as $pjl) : ?>
    		<div class="col-md-12">
    			<table class="table">
    				<tr>
    					<h6>Tanggal Penjualan : <?php echo $pjl->tanggal ?></h6>
                        <h6>Total : <?php echo $pjl->total ?></h6>
    				</tr>
    			</table>
    		</div>
    	</div>

    <?php endforeach; ?>
    </div>
      </div>
</div>