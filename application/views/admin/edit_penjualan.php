<div class="content">
    <div class="container-fluid">
     <div class="row">
      <div class="col-md-12">
        <div class="col-md-12">
         <div class="card card-primary">
          <div class="card-header">
          <h3 class="card-title"><i class="fas fa-edit"></i>Edit Data Penjualan</h3>
      </div>
      

	<?php foreach ($penjualan as $pjl): ?>
		<form method="post" action="<?php echo base_url().'admin/data_penjualan/update' ?>">
			<div class="for-group mt-3">
				<label>Tanggal Penjualan</label>
				<input type="date" name="tanggal" class="form-control" value="<?php echo $pjl->tanggal ?>">
			</div>

			<div class="for-group">
				<label>Total</label>
				<input type="hidden" name="id" class="form-control" value="<?php echo $pjl->id ?>">
				<input type="text" name="total" class="form-control" value="<?php echo $pjl->total ?>">
			</div>
        </div>
		
		<button type="submit" class="btn btn-primary btn-sm mt-3">Update</button>
	</form>

	<?php endforeach; ?>
</div>
</div>
</div>
</div>
</div>
</div>