<div class="content">
    <div class="container-fluid">
     <div class="row">
      <div class="col-md-12">
        <div class="col-md-12">
         <div class="card card-primary">
          <div class="card-header">
          <h3 class="card-title"><i class="fas fa-edit"></i>Edit Data Barang</h3>
      </div>
      

	<?php foreach ($barang as $brg): ?>
		<form method="post" action="<?php echo base_url().'admin/data_barang/update' ?>">
			<div class="for-group mt-3">
				<label>Nama Barang</label>
				<input type="text" name="nama_barang" class="form-control" value="<?php echo $brg->nama_barang ?>">
			</div>

			<div class="for-group">
				<label>Harga</label>
				<input type="hidden" name="id" class="form-control" value="<?php echo $brg->id ?>">
				<input type="text" name="harga" class="form-control" value="<?php echo $brg->harga ?>">
			</div>

      <div class="for-group">
				<label>Stok</label>
				<input type="hidden" name="id" class="form-control" value="<?php echo $brg->id ?>">
				<input type="text" name="stok" class="form-control" value="<?php echo $brg->stok ?>">
			</div>

			<div class="for-group">
            <label>Kategori</label>
            <select class="form-control" name="kategori">
              <option>Makanan</option>
              <option>Gaya Hidup</option>
              <option>Pakaian Wanita</option>
              <option>Pakaian Pria</option>
            </select>         
            	<input type="hidden" name="id" class="form-control" value="<?php echo $brg->id ?>">
          </div>
        </div>
		
		<button type="submit" class="btn btn-primary btn-sm mt-3">Update</button>
	</form>

	<?php endforeach; ?>
</div>
</div>
</div>
</div>
</div>
</div>