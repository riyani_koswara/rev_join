    <div class="container-fluid">
         <?php $this->load->view('menu');?>
    <nav class="navbar">
          <h3><i class="fas fa-box"></i>  Data Barang</h3>
           <div class="card-tools">
      <a class="btn btn-primary btn-sm" href="<?php echo base_url('admin/data_barang/print') ?>">
      <i class="fas fa-print"></i> Print</a>
      <a class="btn btn-warning btn-sm" href="<?php echo base_url('admin/data_barang/pdf') ?>">
      <i class="fas fa-file"></i> Export PDF</a>
      <a class="btn btn-success btn-sm" href="<?php echo base_url('admin/data_barang/excel') ?>">
      <i class="fas fa-file-excel"></i> Export Excel</a>
      <div class="pull-right"><a href="#" class="btn btn-sm btn-default" data-toggle="modal" data-target="#ModalBarang"><span class="fa fa-plus"></span> Tambah Data</a></div>
      </div>
           </nav>

  <div class="row">
    <div class="col-lg-12">
      <div id="reload">
       <table class="table table-striped mt-4" id="mydata">
        <thead>
    <tr>
      <th>Nama Barang</th>
      <th>Harga</th>
      <th>Stok</th>
      <th>Kategori</th>
      <th>Tgl Input</th>
      <th>Aksi</th>    
    </tr>
        </thead>
        <tbody id="show_data">
        
      </tbody>
      </table>
    </div>
  </div>
</div>

        <!-- MODAL ADD -->
        <div class="modal fade" id="ModalBarang" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">INPUT Data Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
            <form class="form-horizontal" id="contact_form">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Barang</label>
                        <div class="col-xs-9">
                            <input name="nama" id="nama_barang" class="form-control" type="text" placeholder="Nama Barang" style="width:335px;">
                           <span id="name_error" class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Harga</label>
                        <div class="col-xs-9">
                            <input name="harga" id="harga" class="form-control" type="number" placeholder="Harga" style="width:335px;">
                            <span id="harga_error" class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Stok</label>
                        <div class="col-xs-9">
                            <input name="stok" id="stok" class="form-control" type="number" placeholder="Stok" style="width:335px;">
                      <span id="stok_error" class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Kategori</label>
                        <div class="col-xs-9">
                            <input name="ktg" id="kategori" class="form-control" type="text" placeholder="Kategori" style="width:335px;">
                         <span id="kategori_error" class="help-block"></span>
                        </div>
                    </div>

                 <div class="form-group">
                        <label class="control-label col-xs-3" >Tanggal Input</label>
                        <div class="col-xs-9">
                            <input name="tg" id="tanggal_input" class="form-control" type="date" placeholder="tanggal input" style="width:335px;">
                             <span id="tanggal_error" class="help-block"></span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <input type="submit" name="btn_simpan" id="btn_simpan" class="btn btn-info" value="Simpan">
                </div>
            </form>
            </div>
          </div>
        </div>
        <!--END MODAL ADD-->


              <!-- MODAL EDIT -->
        <div class="modal fade" id="ModalBarEdit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Edit Data Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form class="form-horizontal" id="barang_edit">
                <div class="modal-body">

                  <div class="form-group">
                        <label class="control-label col-xs-3" >No Barang</label>
                        <div class="col-xs-9">
                            <input name="id_edit" id="id2" class="form-control" type="text" placeholder="No Barang" style="width:335px;" readonly>
                             <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Barang</label>
                        <div class="col-xs-9">
                            <input name="nama_edit" id="nama_barang2" class="form-control" type="text" placeholder="Nama Barang" style="width:335px;">
                             <span id="nama_edit_error" class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Harga</label>
                        <div class="col-xs-9">
                            <input name="harga_edit" id="harga2" class="form-control" type="number" placeholder="Harga" style="width:335px;">
                             <span id="harga_edit_error" class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Stok</label>
                        <div class="col-xs-9">
                            <input name="stok_edit" id="stok2" class="form-control" type="number" placeholder="Stok" style="width:335px;">
                             <span id="stok_edit_error" class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Kategori</label>
                        <div class="col-xs-9">
                            <input name="ktg_edit" id="kategori2" class="form-control" type="text" placeholder="Stok" style="width:335px;">
                             <span id="ktg_edit_error" class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Tanggal Input</label>
                        <div class="col-xs-9">
                            <input name="tg_edit" id="tanggal2" class="form-control" type="date" placeholder="Stok" style="width:335px;" >
                             <span id="tg_edit_error" class="help-block"></span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                        <input type="submit" name="btn_update" id="btn_update" class="btn btn-info" value="Update">      
                </div>
            </form>
        </div>
    </div>
</div>
        <!--END MODAL EDIT-->

                      <!-- MODAL DETAIL -->
        <div class="modal fade" id="ModalBarDetail" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Detail Data Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                  <div class="form-group">
                        <label class="control-label col-xs-3" >No Barang</label>
                        <div class="col-xs-9">
                            <input name="id_detail" class="form-control" type="text" placeholder="No Barang" style="width:335px;" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Barang</label>
                        <div class="col-xs-9">
                            <input name="nama_detail" class="form-control" type="text" placeholder="Nama Barang" style="width:335px;" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Harga</label>
                        <div class="col-xs-9">
                            <input name="harga_detail" class="form-control" type="number" placeholder="Harga" style="width:335px;" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Stok</label>
                        <div class="col-xs-9">
                            <input name="stok_detail" class="form-control" type="number" placeholder="Stok" style="width:335px;" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Kategori</label>
                        <div class="col-xs-9">
                            <input name="ktg_detail" class="form-control" type="text" placeholder="Stok" style="width:335px;" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Tanggal Input</label>
                        <div class="col-xs-9">
                            <input name="tg_detail" class="form-control" type="text" style="width:335px;" readonly>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                </div>
            </form>
            </div>
            </div>
        </div>
        <!--END MODAL DETAIL-->

        <!--MODAL HAPUS-->
        <!-- <div class="modal fade" id="ModalBarHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                    </div>
                    <form class="form-horizontal">
                    <div class="modal-body">
                                          
                      <input type="hidden" name="id" id="textkode" value="">
                    <div class="alert alert-warning">  Apakah Anda yakin mau menghapus data ini?</i></div>
                                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div> -->
        <!--END MODAL HAPUS-->


<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.dataTables.js'?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    tampil_data_barang(); //pemanggilan fungsi tampil barang.
    
    $('#mydata').dataTable();
     
    //fungsi tampil barang
    function tampil_data_barang(){
        $.ajax({
            type  : 'GET',
            url   : '<?php echo base_url()?>admin/data_barang/data_brg',
            async : true,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                          '<td>'+data[i].nama_barang+'</td>'+
                            '<td>'+data[i].harga+'</td>'+
                            '<td>'+data[i].stok+'</td>'+
                            '<td>'+data[i].kategori+'</td>'+
                            '<td>'+data[i].tanggal_input+'</td>'+
                            '<td style="text-align:right;">'+
                                    '<a href="javascript:;" class="btn btn-info btn-sm item_edit" data="'+data[i].id+'"><i class="fas fa-pen-square"></i></a>'+' '+
                                    '<a href="javascript:;" class="btn btn-danger btn-sm item_hapus" data="'+data[i].id+'"><i class="fas fa-trash"></i></a>'+' '+
                                    '<a href="javascript:;" class="btn btn-success btn-sm item_detail" data="'+data[i].id+'"><i class="fas fa-search-plus"></i></a>'+' '+
                                '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });
    }

       //GET UPDATE
    $('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('admin/data_barang/get_brg')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                  $.each(data,function(id, nama_barang, harga, stok, kategori, tanggal_input){
                  $('#ModalBarEdit').modal('show');
                  $('[name="id_edit"]').val(data.id);
                  $('[name="nama_edit"]').val(data.nama_barang);
                  $('[name="harga_edit"]').val(data.harga);
                  $('[name="stok_edit"]').val(data.stok);
                  $('[name="ktg_edit"]').val(data.kategori);
                  $('[name="tg_edit"]').val(data.tanggal_input);
                });
                }
            });
            return false;
        });

        $('#show_data').on('click','.item_hapus',function(){
            var id=$(this).attr("data");
            console.log(id);
            swal({
                title: 'Konfirmasi',
                text: "Anda ingin menghapus ",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                cancelButtonText: 'Tidak',
                reverseButtons: true
              }, function(hapus){
                if (hapus) {
                  $.ajax({
                    url  : "<?php echo base_url('admin/data_barang/hapus_brg')?>", 
                    method:"post",
                    data:{id:id},
                    beforeSend :function () {
                    swal({
                        title: 'Menunggu',
                        html: 'Memproses data',
                        onOpen: () => {
                          swal.showLoading()
                        }
                      })      
                    },    
                    success:function(data){
                      swal(
                        'Hapus',
                        'Berhasil Terhapus',
                        'success'
                      )
                      tampil_data_barang();                   
                       }
                  })
              } else {
                  swal(
                    'Batal',
                    'Anda membatalkan penghapusan',
                    'error'
                  )
                }
              })
            });

    //GET DETAIL
    $('#show_data').on('click','.item_detail',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('admin/data_barang/get_brg')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                  $.each(data,function(id, nama_barang, harga, stok, kategori, tanggal_input){
                  $('#ModalBarDetail').modal('show');
                  $('[name="id_detail"]').val(data.id);
                  $('[name="nama_detail"]').val(data.nama_barang);
                  $('[name="harga_detail"]').val(data.harga);
                  $('[name="stok_detail"]').val(data.stok);
                  $('[name="ktg_detail"]').val(data.kategori);
                  $('[name="tg_detail"]').val(data.tanggal_input);
                });
                }
            });
            return false;
        });


    //GET HAPUS
    $('#show_data').on('click','.item_hapus',function(){
            var id=$(this).attr('data');
            $('#ModalBarHapus').modal('show');
            $('[name="id"]').val(id);
        });

    //Simpan Barang
/*        $('#btn_simpan').on('click',function(){
            var dibar=$('#id').val();
            var nama=$('#nama_barang').val();
            var harga=$('#harga').val();
            var stok=$('#stok').val();
            var ktg=$('#kategori').val();
            var tg=$('#tanggal_input').val();
            $.ajax({
                type : "POST",
                beforeSend :function () {
              swal({
                  title: 'Menunggu',
                  html: 'Memproses data',
                  onOpen: () => {
                    swal.showLoading()
                  }
                })      
              },
                dataType : "JSON",
                success: function(data){
                    swal({
                  type: 'success',
                  title: 'Tambah Barang',
                  text: 'Anda Berhasil Menambah Barang'
                })
                    $('[name="nama"]').val("");
                    $('[name="harga"]').val("");
                    $('[name="stok"]').val("");
                    $('[name="ktg"]').val("");
                    $('[name="tg"]').val("");
                    $('#ModalBarang').modal('hide');
                    tampil_data_barang();
                }
            });
            return false;
        });*/


           //Update Barang
           $('#barang_edit').on('submit', function(event){
  event.preventDefault();
            var dibar=$('#id2').val();
            var nama=$('#nama_barang2').val();
            var harga=$('#harga2').val();
            var stok=$('#stok2').val();
            var ktg=$('#kategori2').val();
            var tg=$('#tanggal2').val();
  $.ajax({
   url  : "<?php echo base_url('admin/data_barang/update_brg')?>",
   method:"POST",
   data:$(this).serialize(),
   dataType:"json",
   beforeSend:function(){
    swal({
                  title: 'Menunggu',
                  html: 'Memproses data',
                  onOpen: () => {
                    swal.showLoading()
                  }
                })
     $('#btn_update').attr('disabled', 'disabled');
   },
   success:function(data)
   {
    if(data.error)
    {
     if(data.nama_edit_error != '')
     {
      $('#nama_edit_error').html(data.nama_edit_error);
     }
     else
     {
      $('#nama_edit_error').html('');
     }
     if(data.harga_edit_error != '')
     {
      $('#harga_edit_error').html(data.harga_edit_error);
     }
     else
     {
      $('#harga_edit_error').html('');
     }
     if(data.stok_error != '')
     {
      $('#stok_edit_error').html(data.stok_edit_error);
     }
     else
     {
      $('#stok_edit_error').html('');
     }
     if(data.ktg_edit_error != '')
     {
      $('#ktg_edit_error').html(data.ktg_edit_error);
     }
     else
     {
      $('#ktg_edit_error').html('');
     }
     if(data.tg_edit_error != '')
     {
      $('#tg_edit_error').html(data.tg_edit_error);
     }
     else
     {
      $('#tg_edit_error').html('');
     }
    }
    if(data.success)
    {
        swal({
                  type: 'success',
                  title: 'Tambah Barang',
                  text: 'Anda Berhasil Mengupdate Barang'
                })
     $('#success_message').html(data.success);
     $('#nama_edit_error').html('');
     $('#harga_edit_error').html('');
     $('#stok_edit_error').html('');
     $('#ktg_edit_error').html('');
     $('#tg_edit_error').html('');
     $('#barang_edit')[0].reset();
     $('#ModalBarEdit').modal('hide');
     tampil_data_barang();
    }
    $('#btn_update').attr('disabled', false);
   }
  })
 });

         //Hapus Barang
        $('#btn_hapus').on('click',function(){
            var id=$('#textkode').val();
            $.ajax({
            type : "POST",
            url  : "<?php echo base_url('admin/data_barang/hapus_brg')?>",
            dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                            $('#ModalBarHapus').modal('hide');
                            tampil_data_barang();
                    }
                });
                return false;
            });


     $('#contact_form').on('submit', function(event){
  event.preventDefault();
            var dibar=$('#id').val();
            var nama=$('#nama_barang').val();
            var harga=$('#harga').val();
            var stok=$('#stok').val();
            var ktg=$('#kategori').val();
            var tg=$('#tanggal_input').val();
  $.ajax({
   url:"<?php echo base_url(); ?>admin/data_barang/simpan_brg",
   method:"POST",
   data:$(this).serialize(),
   dataType:"json",
   beforeSend:function(){
    swal({
                  title: 'Menunggu',
                  html: 'Memproses data',
                  onOpen: () => {
                    swal.showLoading()
                  }
                })
     $('#btn_simpan').attr('disabled', 'disabled');
   },
   success:function(data)
   {
    if(data.error)
    {
     if(data.name_error != '')
     {
      $('#name_error').html(data.name_error);
     }
     else
     {
      $('#name_error').html('');
     }
     if(data.harga_error != '')
     {
      $('#harga_error').html(data.harga_error);
     }
     else
     {
      $('#harga_error').html('');
     }
     if(data.stok_error != '')
     {
      $('#stok_error').html(data.stok_error);
     }
     else
     {
      $('#stok_error').html('');
     }
     if(data.kategori_error != '')
     {
      $('#kategori_error').html(data.kategori_error);
     }
     else
     {
      $('#kategori_error').html('');
     }
     if(data.tanggal_error != '')
     {
      $('#tanggal_error').html(data.tanggal_error);
     }
     else
     {
      $('#tanggal_error').html('');
     }
    }
    if(data.success)
    {
        swal({
                  type: 'success',
                  title: 'Tambah Barang',
                  text: 'Anda Berhasil Menambah Barang'
                })
     $('#success_message').html(data.success);
     $('#name_error').html('');
     $('#harga_error').html('');
     $('#stok_error').html('');
     $('#kategori_error').html('');
     $('#tanggal_error').html('');
     $('#contact_form')[0].reset();
     $('#ModalBarang').modal('hide');
     tampil_data_barang();
    }
    $('#btn_simpan').attr('disabled', false);
   }
  })
 });
 });
</script>
