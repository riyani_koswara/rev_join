      <div class="container-fluid">
         <?php $this->load->view('menu');?>
      <nav class="navbar">
        <center>
          <h3><i class="fas fa-hand-holding-usd"></i>  Data Transaksi</h3>
        </center>
          <div class="card-tools">
           <div class="pull-right"><a href="#" class="btn btn-sm btn-default" data-toggle="modal" data-target="#ModalTransaksi"><span class="fa fa-plus"></span> Tambah Data</a></div>
          </div>
        </nav>

<div class="row">
    <div class="col-lg-12">
      <div id="reload">
       <table class="table table-striped mt-4" id="mydata">
        <thead>
    <tr>
      <th>Nama Konsumen</th>
      <th>Nama Barang</th>
      <th>Harga</th>
      <th>Total Jual</th>
      <th>Jumlah</th>
      <th>Tanggal Transaksi</th>
      <th>Aksi</th>
    </tr>
       </thead>
        <tbody id="show_data">
        
      </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalTransaksi" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">INPUT Data Transaksi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <form class="form-horizontal" id="transaksi_form">
            <div class="form-group">
              <label class="control-label col-xs-3">Nama Pembeli</label>
              <div class="col-xs-9">
                <select class="form-control" name="nama_pembeli" id="nama_pembeli" style="width: 300px; height: 30px;">
                  <?php
          foreach($pembeli as $data){
             echo "<option value='".$data['id_pembeli']."'>".$data['nama_pembeli']."</option>";
      }
           ?>
                </select>
                <span id="name_error" class="help-block"></span>
          </div>
        </div>

                    <div class="form-group">
                <label class="control-label col-xs-3">Nama Barang</label>
                 <div class="col-xs-9">
         <select class="form-control" name="nama_barang" id="nama_barang" style="width: 300px; height: 30px;">
                           <?php
          foreach($barang as $data){
             echo "<option value='".$data['id']."'>".$data['nama_barang']."</option>";
      }
           ?> 
           </select>               
           <span id="barang_error" class="help-block"></span>   
              </div>
            </div>

                 <div class="form-group">
            <label class="control-label col-xs-3">Harga</label>
             <div class="col-xs-9">
          <select class="form-control" name="harga" id="harga" style="width: 300px; height: 30px;" readonly="">
           <option></option>
         </select>
          <span id="harga_error" class="help-block"></span>
              </div>
            </div>

                 <div class="form-group">
              <label class="control-label col-xs-3">Jumlah</label>
            <div class="col-xs-9">
          <input type="number" name="jumlah" id="jumlah" class="form-control" style="width: 300px; height: 30px;" >
          <span id="jumlah_error" class="help-block"></span>
                </div>
              </div>

         <div class="form-group">
            <label class="control-label col-xs-3">Total</label>
          <div class="col-xs-9">
          <input type="number" name="total" id="total" class="form-control" style="width: 300px; height: 30px;" >
          <span id="total_error" class="help-block"></span>
        </div>
      </div>
                  <div class="form-group">
            <label class="control-label col-xs-3">Tanggal</label>
          <div class="col-xs-9">
          <input type="date" name="tanggal" id="tanggal" class="form-control" style="width: 300px; height: 30px;" >
          <span id="total_error" class="help-block"></span>
        </div>
      </div>

 <button type="button" class="btn btn-success" id="add" style="margin-left: 330px;"> 
  <i class="fas fa-plus"></i>  Add</button>
     </div>

       <div class="form-group">
        <center>
       <table border="1" cellspacing="0" id="table_sementara">
  <tr>
    <th>Nama Barang</th>
    <th>Harga</th>
    <th>Jumlah</th>
    <th>Total</th>
  </tr>
        </table>
      </center>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <input type="submit" name="btn_simpan" id="btn_simpan" class="btn btn-info" value="Simpan">
      </div>
  </form>
    </div>
  </div>
</div>
</div>

        <!-- MODAL EDIT -->
        <div class="modal fade" id="ModalTranEdit" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Edit Data Transaksi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
            <form class="form-horizontal" id="barang_edit">
                <div class="modal-body">

                  <div class="form-group">
                        <label class="control-label col-xs-3" >No Transaksi</label>
                        <div class="col-xs-9">
                            <input name="id_edit" id="id_pembeli" class="form-control" type="text" placeholder="No Konsumen" style="width:335px;" readonly>
                        </div>
                    </div>

                <div class="form-group">
              <label class="control-label col-xs-3">Nama Pembeli</label>
                <select class="form-control" name="nama_pembeli" id="nama_pembeli" style="width: 300px; height: 30px;">
                  <?php
          foreach($pembeli as $data){
             echo "<option value='".$data['id_pembeli']."'>".$data['nama_pembeli']."</option>";
      }
           ?>
                </select>
                <span id="name_edit_error" class="help-block"></span>
          </div>

                    <div class="form-group">
                <label class="control-label col-xs-3">Nama Barang</label>
         <select class="form-control" name="nama_barang" id="nama_barang" style="width: 300px; height: 30px;">
          <option value=""></option>
                </select>
                <span id="barang_edit_error" class="help-block"></span>   
              </div>

                 <div class="form-group">
            <label class="control-label col-xs-3">Harga</label>
          <select class="form-control harga_edit" name="harga" id="harga" style="width: 300px; height: 30px;">
           <option></option>
         </select>
          <span id="harga_edit_error" class="help-block"></span>
              </div>

          <div class="form-group">
        <label class="control-label col-xs-3">Jumlah</label>
        <div class="col-xs-9">
         <input type="number" name="jumlah_edit" id="jumlah1" class="form-control">
         <span id="jumlah_edit_error" class="help-block"></span>
      </div>
    </div>

      <div class="form-group">
        <label class="control-label col-xs-3">Tanggal Transaksi</label>
        <div class="col-xs-9">
        <input type="date" name="tanggal_edit" id="tanggal1" class="form-control">
        <span id="tanggal_edit_error" class="help-block"></span>
      </div>
    </div>

      <div class="form-group">
        <label class="control-label col-xs-3">Total</label>
        <div class="col-xs-9">
        <input type="number" name="total_edit" id="total1" class="form-control">
        <span id="total_edit_error" class="help-block"></span>
      </div>
    </div>
  </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
              <input type="submit" name="btn_update" id="btn_update" class="btn btn-info" value="Update">
                </div>
            </form>
          </div>
        </div>
      </div>


        <!-- MODAL DETAIL -->
        <!--END MODAL DETAIL-->

        <!--MODAL HAPUS-->
        <!-- <div class="modal fade" id="ModalTranHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        
                    </div>
                    <form class="form-horizontal">
                    <div class="modal-body">
                                          
                            <input type="hidden" name="id_pembeli" id="textkode" value="">
                            <div class="alert alert-warning"> Apakah Anda yakin mau menghapus data ini?</i></div>
                                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div> -->
        <!--END MODAL HAPUS-->

        <!-- akhir kode modal dialog -->

<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.dataTables.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/sweetalert/sweetalert.min.js'?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    tampil_data_barang(); //pemanggilan fungsi tampil barang.
    
    $('#mydata').dataTable();
     
    //fungsi tampil barang
    function tampil_data_barang(){
        $.ajax({
            type  : 'GET',
            url   : '<?php echo base_url()?>admin/data_transaksi/transaksi',
            async : true,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                          '<td>'+data[i].nama_pembeli+'</td>'+
                            '<td>'+data[i].nama_barang+'</td>'+
                            '<td>'+data[i].harga+'</td>'+
                            '<td>'+data[i].total+'</td>'+
                            '<td>'+data[i].jumlah+'</td>'+
                            '<td>'+data[i].tanggal+'</td>'+
                                '<td style="text-align:right;">'+
                                    /*'<a href="javascript:;" class="btn btn-info btn-sm item_edit" data="'+data[i].id_pembeli+'"><i class="fas fa-pen-square"></i></a>'+' '+*/
                                    '<a href="javascript:;" class="btn btn-danger btn-sm item_hapus" data="'+data[i].id_pembeli+'"><i class="fas fa-trash"></i></a>'+' '+
/*                                    '<a href="javascript:;" class="btn btn-success btn-sm item_detail" data="'+data[i].id+'"><i class="fas fa-search-plus"></i></a>'+' '+
*/                                '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });
    }

    //GET UPDATE
    $('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('admin/data_transaksi/get_trans')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                $.each(data,function(id_pembeli, nama_pembeli, nama_barang, harga, jumlah, tanggal, total){
                  $('#ModalTranEdit').modal('show');
                  $('[name="id_edit"]').val(data.id_pembeli);
                  $('[name="nama_pembeli"]').val(data.nama_pembeli);
                  $('[name="nama_barang"]').val(data.nama_barang);
                  $('[name="harga"]').val(data.harga);
                  $('[name="jumlah_edit"]').val(data.jumlah);
                  $('[name="tanggal_edit"]').val(data.tanggal);
                  $('[name="total_edit"]').val(data.total);
                });

                $('[name="nama_pembeli"]').change(function(){
                  $("#nama_barang").hide();
                
                  $.ajax({
                    type: "POST", 
                    url: "<?php echo base_url("admin/data_transaksi/listBarang"); ?>",
                    data: {id_pembeli : $(this).val()},
                    dataType: "json",
                    beforeSend: function(e) {
                      if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                      }
                    },
                    success: function(response){ 
                      $('[name="nama_barang"]').html(response.list_barang).show();
                    },
                    error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                      alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                  });
                });

                    $('[name="nama_pembeli"]').change(function(){
                    $("#harga").hide();
                  
                    $.ajax({
                      type: "POST", 
                      url: "<?php echo base_url("admin/data_transaksi/listHarga"); ?>",
                      data: {id_pembeli : $(this).val()},
                      dataType: "json",
                      beforeSend: function(e) {
                        if(e && e.overrideMimeType) {
                          e.overrideMimeType("application/json;charset=UTF-8");
                        }
                      },
                      success: function(response){ 
                        console.log(response)
                        $('[name="harga"]').html(response.list_harga).show();
                      },
                      error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                      }
                    });
                  });

                    $('input[name=jumlah_edit]').on('keyup' ,function() {
                      var jumlah = $(this).val();
                      var harga = $('.harga_edit').val();
                      $('input[name=total_edit]').val(harga * jumlah);
                  } ) ;
                }
            });
        });

   //GET HAPUS
    $('#show_data').on('click','.item_hapus',function(){
            var id_pembeli=$(this).attr("data");
            swal({
                title: 'Konfirmasi',
                text: "Anda ingin menghapus ",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                cancelButtonText: 'Tidak',
                reverseButtons: true
              }, function(hapus){
                if (hapus) {
                  $.ajax({
                    url  : "<?php echo base_url('admin/data_transaksi/hapus')?>",
                    method:"post",
                    data:{id_pembeli:id_pembeli},
                    beforeSend :function () {
                    swal({
                        title: 'Menunggu',
                        html: 'Memproses data',
                        onOpen: () => {
                          swal.showLoading()
                        }
                      })      
                    },    
                    success:function(data){
                      swal(
                        'Hapus',
                        'Berhasil Terhapus',
                        'success'
                      )
                      tampil_data_barang();                   
                   }
                  })
              } else {
                  swal(
                    'Batal',
                    'Anda membatalkan penghapusan',
                    'error'
                  )
                }
              })
            });


    //Simpan Barang
         $('#transaksi_form').on('submit', function(event){
  event.preventDefault();
            var nama_pembeli=$('#nama_pembeli').val();
            var nama_barang=$('#nama_barang').val();
            var harga=$('#harga').val();
            var total=$('#total').val();
            var jumlah=$('#jumlah').val();
            var tanggal=$('#tanggal').val();
  $.ajax({
   url  : "<?php echo base_url('admin/data_transaksi/tambah_aksi')?>",
   method:"POST",
   data:$(this).serialize(),
   dataType:"json",
   beforeSend:function(){
/*    Swal({
                  title: 'Menunggu',
                  html: 'Memproses data',
                  onOpen: () => {
                    swal.showLoading()
                  }
                })*/
     $('#btn_simpan').attr('disabled', 'disabled');
   },
   success:function(data)
   {
    if(data.error)
    {
     if(data.name_error != '')
     {
      $('#name_error').html(data.name_error);
     }
     else
     {
      $('#name_error').html('');
     }
     if(data.barang_error != '')
     {
      $('#barang_error').html(data.barang_error);
     }
     else
     {
      $('#barang_error').html('');
     }
     if(data.harga_error != '')
     {
      $('#harga_error').html(data.harga_error);
     }
     else
     {
      $('#harga_error').html('');
     }
     if(data.jumlah_error != '')
     {
      $('#jumlah_error').html(data.jumlah_error);
     }
     else
     {
      $('#jumlah_error').html('');
     }
     if(data.tanggal_error != '')
     {
      $('#tanggal_error').html(data.tanggal_error);
     }
     else
     {
      $('#tanggal_error').html('');
     }
     if(data.total_error != '')
     {
      $('#total_error').html(data.total_error);
     }
     else
     {
      $('#total_error').html('');
     }
    }
    if(data.success)
    {
        swal({
                  type: 'success',
                  title: 'Tambah Barang',
                  text: 'Anda Berhasil Menambah Pembeli'
                })
     $('#success_message').html(data.success);
     $('#name_error').html('');
     $('#barang_error').html('');
     $('#harga_error').html('');
     $('#jumlah_error').html('');
     $('#tanggal_error').html('');
     $('#total_error').html('');
     $('#transaksi_form')[0].reset();
     $('#ModalTransaksi').modal('hide');
     tampil_data_barang();
    }
    $('#btn_simpan').attr('disabled', false);
   }
  })
 });


        //Update Barang
         $('#barang_edit').on('submit', function(event){
  event.preventDefault();
            var id_pembeli=$('ModalTranEdit #id_edit').val();
            var nama_pembeli=$('ModalTranEdit #nama_pembeli').val();
            var nama_barang=$('ModalTranEdit #nama_barang').val();
            var harga=$('ModalTranEdit #harga').val();
            var total=$('ModalTranEdit #total_edit').val();
            var jumlah=$('ModalTranEdit #jumlah_edit').val();
            var tanggal=$('ModalTranEdit #tanggal_edit').val();
  $.ajax({
    url  : "<?php echo base_url('admin/data_transaksi/update')?>",
   method:"POST",
   data:$(this).serialize(),
   dataType:"json",
   beforeSend:function(){
    swal({
                  title: 'Menunggu',
                  html: 'Memproses data',
                  onOpen: () => {
                    swal.showLoading()
                  }
                })
     $('#btn_update').attr('disabled', 'disabled');
   },
   success:function(data)
   {
    if(data.error)
    {
     if(data.nali_edit_error != '')
     {
      $('#nali_edit_error').html(data.nali_edit_error);
     }
     else
     {
      $('#nali_edit_error').html('');
     }
     if(data.nabar_edit_error != '')
     {
      $('#nabar_edit_error').html(data.nabar_edit_error);
     }
     else
     {
      $('#nabar_edit_error').html('');
     }
     if(data.harga_edit_error != '')
     {
      $('#harga_edit_error').html(data.harga_edit_error);
     }
     else
     {
      $('#harga_edit_error').html('');
     }
     if(data.jumlah_edit_error != '')
     {
      $('#jumlah_edit_error').html(data.jumlah_edit_error);
     }
     else
     {
      $('#jumlah_edit_error').html('');
     }
     if(data.tanggal_edit_error != '')
     {
      $('#tanggal_edit_error').html(data.tanggal_edit_error);
     }
     else
     {
      $('#tanggal_edit_error').html('');
     }
     if(data.total_edit_error != '')
     {
      $('#total_edit_error').html(data.total_edit_error);
     }
     else
     {
      $('#total_edit_error').html('');
     }
    }
    if(data.success)
    {
        swal({
                  type: 'success',
                  title: 'Update Barang',
                  text: 'Anda Berhasil Mengubah Pembeli'
                })
     $('#success_message').html(data.success);
     $('#nali_edit_error').html('');
     $('#nabar_edit_error').html('');
     $('#harga_edit_error').html('');
     $('#jumlah_edit_error').html('');
     $('#tanggal_edit_error').html('');
     $('#total_edit_error').html('');
     $('#barang_edit')[0].reset();
     $('#ModalTranEdit').modal('hide');
     tampil_data_barang();
    }
    $('#btn_update').attr('disabled', false);
   }
  })
 });

/*      $("#nama_pembeli").change(function(){
      $("#nama_barang").hide();
    
      $.ajax({
        type: "POST", 
        data: {id_pembeli : $("#nama_pembeli").val()},
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ 
          $("#nama_barang").html(response.list_barang).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });
*/
      $("#nama_barang").change(function(){
      $("#harga").hide();
    
      $.ajax({
        type: "POST", 
        url: "<?php echo base_url("admin/data_transaksi/listHarga"); ?>",
        data: {id : $("#nama_barang").val()},
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ 
            $("#harga").html(response.list_harga).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });

      $('input[name=jumlah]').on('keyup' ,function() {
        var jumlah = $('input[name=jumlah]').val();
        var harga = $('select[name=harga]').val();
        $('input[name=total]').val(harga * jumlah);
    } ) ;


  $("delete").on('click', function() {
  $('.case:checkbox:checked').parents("tr").remove();
    $('.check_all').prop("checked", false); 
  check();
});

    $('#add').click(function(){
      var nama_barang = $('select[name=nama_barang]').val();
      var harga = $('#harga').val();
      var jumlah = $('#jumlah').val();
      var total = $('#total').val();

      $('#table_sementara tbody:last-child').append(
        '<tr>'+
        '<td>'+nama_barang+'</td>'+
        '<td>'+harga+'</td>'+
        '<td>'+jumlah+'</td>'+
        '<td>'+total+'</td>'+
        '</tr>'
        );
    });

    function select_all() {
  $('input[class=case]:checkbox').each(function(){ 
    if($('input[class=check_all]:checkbox:checked').length == 0){ 
      $(this).prop("checked", false); 
    } else {
      $(this).prop("checked", true); 
    } 
  });
}
  });
</script>