<div class="content">
    <div class="container-fluid">
     <div class="row">
      <div class="col-md-12">
        <div class="col-md-12">
         <div class="card card-primary">
          <div class="card-header">
          <h3 class="card-title"><i class="fas fa-edit"></i>Edit Data Transaksi</h3>
      </div>
		<form method="post" action="<?php echo base_url().'admin/Data_transaksi/update' ?>">
			<div class="for-group mt-3">
			<label>Nama Konsumen</label>
          <select class="form-control" name="nama_pembeli">
                  <?php foreach($pembeli as $l){ ?>
                  <option value="<?php echo $l['id_pembeli']; ?>"><?php echo $l['nama_pembeli']; ?></option>
                  <?php } ?>
                </select>

                  <label>Nama Barang</label>
          <select class="form-control" name="nama_barang" id="kategori">
                  <?php foreach($barang as $a){ ?>
                  <option value="<?php echo $a['id']; ?>"><?php echo $a['nama_barang']; ?>   </option>
                  <?php } ?>
                </select>

          		<label>Harga</label>
          <select class="form-control" name="harga">
                  <option value="0">-PILIH-</option>
          </select>

          <?php foreach ($transaksi as $tks) : ?>

			<div class="for-group">
				<label>Jumlah</label>
			<input type="hidden" name="id_pembeli" class="form-control" value="<?php echo $tks->id_pembeli ?>">
				<input type="text" name="jumlah" class="form-control" value="<?php echo $tks->jumlah ?>">
			</div>

			<div class="for-group">
				<label>Tanggal Transaksi</label>
			<input type="hidden" name="id_pembeli" class="form-control" value="<?php echo $tks->id_pembeli ?>">
				<input type="date" name="tanggal" class="form-control" value="<?php echo $tks->tanggal ?>">
			</div>

			<div class="for-group">
				<label>Total</label>
			<input type="hidden" name="id_pembeli" class="form-control" value="<?php echo $tks->id_pembeli ?>">
				<input type="text" name="total" class="form-control" value="<?php echo $tks->total ?>">
			</div>
        </div>
		
		<button type="submit" class="btn btn-primary btn-sm mt-3">Update</button>
	</form>     
          <?php endforeach; ?>
</div>
</div>
</div>
</div>
</div>
</div>