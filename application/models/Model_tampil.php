<?php 
class Model_tampil extends CI_Model{
	public function __construct()
	{
    	parent::__construct();
	}

	function get_pembeli()
	{
 return $this->db->get('tb_pembeli')->result_array();
	}

	public function edit_tampil($where,$table){
		  return $this->db->get_where($table,$where);
	  }

	public function viewBarang($id){
    $this->db->where('id', $id);
    $result = $this->db->get('tb_barang')->result();
    
    return $result;
}
}
?>