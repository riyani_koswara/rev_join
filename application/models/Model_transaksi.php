<?php 
class Model_transaksi extends CI_Model{
	public function tampil_data(){
		if (!empty($id_pembeli)){
			$this->db->where('tb_pembeli.id_pembeli', $id_pembeli);
		}

		$this->db->select(array(
			'tb_pembeli.*',
			'tb_transaksi.total',
			'tb_transaksi.jumlah',
			'tb_transaksi.tanggal',
			'tb_barang.nama_barang',
			'tb_barang.harga',
		));
		$this->db->from('tb_pembeli');
		$this->db->join('tb_transaksi','tb_transaksi.id_pembeli=tb_pembeli.id_pembeli');
		$this->db->join('tb_barang','tb_barang.id=tb_transaksi.id_pembeli');
		$data = $this->db->get();
		return $data->result();
	}

	function simpan_transaksi($id_pembeli,$jumlah,$total,$tanggal){
		$hasil=$this->db->query("INSERT INTO tb_transaksi (id_pembeli,jumlah,total,tanggal)VALUES('$id_pembeli','$jumlah','$total','$tanggal')");
		return $hasil;
	}

	function get_transaksi($id_pembeli){
				$hsl=$this->db->query("SELECT * FROM tb_transaksi WHERE id_pembeli='$id_pembeli'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil = array(
					'id_pembeli' => $data->id_pembeli,
					'jumlah' => $data->jumlah,
					'total' => $data->total,
					'tanggal' => $data->tanggal,
					);
			}
		}
		return $hasil;
    }

function update_transaksi($id_pembeli,$jumlah,$total,$tanggal){
	$hasil=$this->db->query("UPDATE tb_transaksi SET jumlah='$jumlah',total='$total',tanggal='$tanggal' WHERE id_pembeli='$id_pembeli'");
		return $hasil;
	}
  
	function hapus_transaksi($id_pembeli){
		$this->db->where('id_pembeli',$id_pembeli);
        return $this->db->delete('tb_transaksi');
	}

	function detail_transaksi($id_pembeli){
				$hsl=$this->db->query("SELECT * FROM tb_transaksi WHERE id_pembeli='$id_pembeli'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil = array(
					'id_pembeli' => $data->id_pembeli,
					'jumlah' => $data->jumlah,
					'total' => $data->total,
					'tanggal' => $data->tanggal,
					);
			}
		}
    }

	/*public function edit_transaksi($where,$table){
		  return $this->db->get_where($table,$where);
	  }
  
	  public function update_data($where,$data,$table){
		  $this->db->where($where);
		  $this->db->update($table,$data);
	  }*/
	}

	function Pembeli()
    {
        $this->db->order_by('id_pembeli', 'ASC');
        return $this->db->from('tb_pembeli')
          ->get()
          ->result();
    }

    function Barang($id_pembeli)
    {
        $this->db->where('id_pembeli', $id_pembeli);
        $this->db->order_by('id', 'ASC');
        return $this->db->from('tb_barang')
            ->get()
            ->result();
    }

?>