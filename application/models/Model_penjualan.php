<?php 
	class Model_penjualan extends CI_Model{
		public function tampil_data(){
			$this->db->order_by('id', 'DESC');
		  return $this->db->get('tb_transaksi');
	  }
  public function tambah_penjualan($data,$table){
		  $this->db->insert($table,$data);
	  }
  
	  public function edit_penjualan($where,$table){
		  return $this->db->get_where($table,$where);
	  }
  
	  public function update_data($where,$data,$table){
		  $this->db->where($where);
		  $this->db->update($table,$data);
	  }
  
	  public function hapus_data($where,$table){
		  $this->db->where($where);
		  $this->db->delete($table);
	  }
  
	  public function detail_penjualan($id){
		  $result= $this->db->where('id', $id)->get('tb_transaksi');
		  if ($result->num_rows() > 0){
			  return $result->result();
		  }else{
			  return false;
		  }
		}

	public function get_keyword($keyword){
		$this->db->select('*');
		$this->db->from('tb_transaksi');
		$this->db->like('tanggal', $keyword);
		$this->db->or_like('total', $keyword);
		return $this->db->get()->result();
	}

	}
 ?>