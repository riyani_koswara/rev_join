<?php 
class Model_baba extends CI_Model{
	public function __construct()
	{
    	parent::__construct();
	}

	function get_barang()
	{
    $query = $this->db->get('tb_barang');
    return $query->result_array();
	}

	function tampil(){
		if (!empty($id_pembeli)){
			$this->db->where('tb_pembeli.id_pembeli', $id_pembeli);
		}

		$this->db->select(array(
			'tb_pembeli.*',
			'tb_transaksi.total',
			'tb_transaksi.jumlah',
			'tb_transaksi.tanggal',
			'tb_barang.nama_barang',
			'tb_barang.harga',
		));
		$this->db->from('tb_pembeli');
		$this->db->join('tb_transaksi','tb_transaksi.id_pembeli=tb_pembeli.id_pembeli');
		$this->db->join('tb_barang','tb_barang.id=tb_transaksi.id_pembeli');
		$data = $this->db->get();
		return $data->result();
	}

	public function edit_baba($apa,$table){
		  return $this->db->get_where($table,$apa);
	  }
}
?>