<?php 
	class Model_barang extends CI_Model{
		function barang_list(){
		$hasil=$this->db->query("SELECT * FROM tb_barang");
		return $hasil->result();
	}

	function simpan_barang($dibar,$nama,$harga,$stok,$ktg,$tg){
		$barang=$this->db->query("INSERT INTO tb_barang (id,nama_barang,harga,stok,kategori,tanggal_input)VALUES('$dibar','$nama','$harga','$stok','$ktg','$tg')");
		return $barang;	
	}

	function get_barang_by_kode($dibar){
		$hsl=$this->db->query("SELECT * FROM tb_barang WHERE id='$dibar'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'id' => $data->id,
					'nama_barang' => $data->nama_barang,
					'harga' => $data->harga,
					'stok' => $data->stok,
					'kategori' => $data->kategori,
					'tanggal_input' => $data->tanggal_input
					);
			}
		}
		return $hasil;
	}

	function update_barang($dibar,$nama,$harga,$stok,$ktg,$tg){
		$hasil=$this->db->query("UPDATE tb_barang SET nama_barang='$nama',harga='$harga',stok='$stok',kategori='$ktg',tanggal_input='$tg' WHERE id='$dibar'");
		return $hasil;
	}

	function hapus_barang($dibar){
		$hasil=$this->db->query("DELETE FROM tb_barang WHERE id='$dibar'");
		return $hasil;
	}
	  public function detail_barang($id){
		  $result= $this->db->where('id', $id)->get('tb_barang');
		  if ($result->num_rows() > 0){
			  return $result->result();
		  }else{
			  return false;
		  }
		}

	public function get_keyword($keyword){
		$this->db->select('*');
		$this->db->from('tb_barang');
		$this->db->like('nama_barang', $keyword);
		$this->db->or_like('harga', $keyword);
		$this->db->or_like('stok', $keyword);
		$this->db->or_like('kategori', $keyword);
		return $this->db->get()->result();
	}

	public function save($data){
		 return $this->db->insert('tb_barang', $data);
	} 

	}
 ?>