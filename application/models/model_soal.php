<?php 

class Model_soal extends CI_Model{
	public function tampil_data(){
		$this->db->order_by('id_soal', 'DESC');
		return $this->db->get('tb_soal');
	}
public function tambah_soal($data,$table){
		$this->db->insert($table,$data);
	}

	public function edit_soal($where,$table){
		return $this->db->get_where($table,$where);
	}

	public function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	public function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function find($id_soal){
		$result =$this->db->where('id_soal', $id_soal)
						  ->limit(1)
						  ->get('tb_soal');
			if ($result->num_rows() > 0){
				return $result->row();
			}else{
				return array();
			}
	}

	public function latihan(){
		return $this->db->get('tb_soal');
	}
	public function detail_soal($id_soal){
		$result= $this->db->where('id_soal', $id_soal)->get('tb_soal');
		if ($result->num_rows() > 0){
			return $result->result();
		}else{
			return false;
		}
	}
}