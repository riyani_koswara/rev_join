<?php 
	class Model_pembeli extends CI_Model{
	function pembeli_list(){
		$hasil=$this->db->query("SELECT * FROM tb_pembeli");
		return $hasil->result();
	}

	function simpan_pembeli($kode,$nama,$alam,$jk,$email){
		$hasil=$this->db->query("INSERT INTO tb_pembeli (id_pembeli,nama_pembeli,alamat,jenis_kelamin,email)VALUES('$kode','$nama','$alam','$jk','$email')");
		return $hasil;
	}

	function get_pembeli_by_kode($kode){
		$hsl=$this->db->query("SELECT * FROM tb_pembeli WHERE id_pembeli='$kode'");
		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'id_pembeli' => $data->id_pembeli,
					'nama_pembeli' => $data->nama_pembeli,
					'alamat' => $data->alamat,
					'jenis_kelamin' => $data->jenis_kelamin,
					'email' => $data->email
					);
			}
		}
		return $hasil;
	}

	function update($kode,$nama,$alam,$jk,$email){
		$hasil=$this->db->query("UPDATE tb_pembeli SET nama_pembeli='$nama',alamat='$alam',
			jenis_kelamin='$jk',email='$email' WHERE id_pembeli='$kode'");
		return $hasil;
	}

	function hapus_pembeli($kode){
		 $this->db->where('id_pembeli',$kode);
        return $this->db->delete('tb_pembeli');
    }
	  public function detail_pembeli($kode){
		  $result= $this->db->where('id_pembeli', $kode)->get('tb_pembeli');
		  if ($result->num_rows() > 0){
			  return $result->result();
		  }else{
			  return false;
		  }
		}

	public function get_keyword($keyword){
		$this->db->select('*');
		$this->db->from('tb_pembeli');
		$this->db->like('nama_pembeli', $keyword);
		$this->db->or_like('alamat', $keyword);
		$this->db->or_like('email', $keyword);
		return $this->db->get()->result();
	}

function check_email($email){
   $this->db->select('email');
   $this->db->where('email',$email);		
   $query =$this->db->get('tb_pembeli');
   $row = $query->row();
   if ($query->num_rows > 0){
         return $row->email; 
   }else{
         return "";
  }
}

	}
 ?>