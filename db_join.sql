-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 26 Jun 2020 pada 04.52
-- Versi server: 5.7.26-0ubuntu0.18.04.1-log
-- Versi PHP: 7.3.6-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_join`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id` int(11) NOT NULL,
  `id_pembeli` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `tanggal_input` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_barang`
--

INSERT INTO `tb_barang` (`id`, `id_pembeli`, `nama_barang`, `harga`, `stok`, `kategori`, `tanggal_input`) VALUES
(1, 1, 'Sepatu', 200000, 500, 'Fashion', '2020-03-12'),
(2, 2, 'Jaket', 250000, 215, 'Fashion', '2020-03-13'),
(3, 3, 'Kemeja', 150000, 231, 'Gaya Hidup', '2020-03-18'),
(4, 4, 'Hijab Pasmina', 50000, 221, 'Fashion', '2020-03-13'),
(5, 5, 'Susu SGM', 80000, 2312, 'Makanan', '2020-03-11'),
(6, 6, 'Sapu', 10000, 3546, 'Peralatan Rumah', '2020-03-03'),
(7, 7, 'Laptop ASUS X441B', 5000000, 21, 'Elektronik', '2020-03-02'),
(8, 8, 'Cimoring', 10000, 5675, 'Makanan', '2020-03-18'),
(9, 9, 'Masker', 2000, 456, 'Kesehatan', '2020-03-04'),
(10, 10, 'Mie Lidi', 7000, 2345, 'Makanan', '2020-03-02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_transaksi`
--

CREATE TABLE `tb_detail_transaksi` (
  `id_detail_transaksi` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kasir`
--

CREATE TABLE `tb_kasir` (
  `id` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `passwoard` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kasir`
--

INSERT INTO `tb_kasir` (`id`, `nama`, `passwoard`) VALUES
(1, 'kasir', 'c7911af3adbd12a035b289556d96470a');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_manager`
--

CREATE TABLE `tb_manager` (
  `id` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `passwoard` varchar(225) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_manager`
--

INSERT INTO `tb_manager` (`id`, `nama`, `passwoard`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
(2, 'manager', '1d0258c2440a8d19e716292b231e3190', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_package`
--

CREATE TABLE `tb_package` (
  `package_id` int(11) NOT NULL,
  `nama_package` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pembeli`
--

CREATE TABLE `tb_pembeli` (
  `id_pembeli` int(11) NOT NULL,
  `nama_pembeli` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `jenis_kelamin` varchar(225) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pembeli`
--

INSERT INTO `tb_pembeli` (`id_pembeli`, `nama_pembeli`, `alamat`, `jenis_kelamin`, `email`) VALUES
(1, 'Riyani Koswara', 'Badakarya', 'Perempuan', 'riyanikoswara8@gmail.com'),
(2, 'Ratih Koswara', 'Tanggerang', 'Perempuan', 'ratihkoswara@gmail.com'),
(3, 'Momon Koswara', 'Bandung', 'Laki-laki', 'momon@gmail.com'),
(4, 'Suratmi', 'Bogor', 'Perempuan', 'suratmi@gmail.com'),
(5, 'Elsa Hikmah Purnama', 'Badakarya', 'Perempuan', 'enca@gmail.com'),
(6, 'Atot Supratman', 'Purwokerto', 'Perempuan', 'atot@gmail.com'),
(7, 'Yumna', 'Badakarya', 'Perempuan', 'yumna@gmail.com'),
(8, 'Dian Prasetya', 'Yogyakarta', 'Perempuan', 'dian@gmail.com'),
(9, 'Siti Aisyah', 'Yogyakarta', 'Perempuan', 'siti@gmail.com'),
(10, 'Sabrina', 'Yogyakarta', 'Perempuan', 'sabrina@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id_pembeli` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id_pembeli`, `tanggal`, `total`, `jumlah`) VALUES
(1, '2020-04-06', 150000, 3),
(3, '2020-06-03', 750000, 3),
(4, '2020-04-06', 150000, 3);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_detail_transaksi`
--
ALTER TABLE `tb_detail_transaksi`
  ADD PRIMARY KEY (`id_detail_transaksi`);

--
-- Indeks untuk tabel `tb_kasir`
--
ALTER TABLE `tb_kasir`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_manager`
--
ALTER TABLE `tb_manager`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_package`
--
ALTER TABLE `tb_package`
  ADD PRIMARY KEY (`package_id`);

--
-- Indeks untuk tabel `tb_pembeli`
--
ALTER TABLE `tb_pembeli`
  ADD PRIMARY KEY (`id_pembeli`);

--
-- Indeks untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id_pembeli`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_barang`
--
ALTER TABLE `tb_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tb_detail_transaksi`
--
ALTER TABLE `tb_detail_transaksi`
  MODIFY `id_detail_transaksi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_kasir`
--
ALTER TABLE `tb_kasir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_manager`
--
ALTER TABLE `tb_manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_package`
--
ALTER TABLE `tb_package`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_pembeli`
--
ALTER TABLE `tb_pembeli`
  MODIFY `id_pembeli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id_pembeli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
